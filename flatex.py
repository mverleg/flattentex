
from os.path import dirname, abspath
from sys import stderr, argv
from os import environ
from re import sub, split, findall
from texinputs import path_to_dirs, open_from_dirs


def flatten(tex_file, path, add_comments = True, include_as_input = False, continue_on_missing = False, exclude_extensions = ['.pgf']):
	"""
		Flatten a file, resolving any inputs or includes.

		:param tex_file: file to resolve all includes and imports of
		:param add_comments: it True, includes comments to indicate where changes have been made
		:param include_as_input: treat includes and inputs (so don't create whitespace, don't limit recursion and
		ignore includeonly)
		:param continue_on_missing: show a warning but continue when an imported file isn't found
		:return:
	"""
	path_dirs = path_to_dirs(path)
	with open(tex_file, 'r') as fh:
		content = fh.read()
		includeonly_strs = findall(r'\n[^%]*(\\includeonly{[^}]*})', content)
		for incl_str in includeonly_strs:
			content = content.replace(incl_str, '%% %s used to be here' % incl_str if add_comments else '')
		includeonly = [incl.strip() for incl in findall(r'{([^}]*)}', includeonly_strs[-1])[-1].split(',')] if includeonly_strs else None
		return _flatten_text(content, dirs = path_dirs, add_comments = add_comments, include_as_input =
			include_as_input, continue_on_missing = continue_on_missing, includeonly = includeonly, exclude_extensions = exclude_extensions)


def _flatten_text(text, dirs, add_comments, include_as_input, continue_on_missing, includeonly, exclude_extensions = ['.pgf'], is_included = False):
	"""
		Flatten a text, recursively calling _flatten_text on any inputs or includes.

		:param is_included: are we already inside an include? (keep track because includes cannot be nested)
	"""
	""" split into text blocks seperated by where include statements used to be """
	parts = split(r'\\(input|include){([^}]*)}', text)
	for nr in range(0, len(parts), 3)[:-1]:
		method = parts[nr + 1]
		incl_filename = sub(r'%[^\n]*\n', '', parts[nr + 2]).strip()
		""" check if extension ignored """
		if any(incl_filename.endswith(ext) for ext in exclude_extensions):
			parts[nr + 1] = ''
			parts[nr + 2] = '\%s{%s}' % (method, incl_filename)
			continue
		""" check if this input is not commented out """
		if '%' in parts[nr].splitlines()[-1].replace('\%', ''):
			parts[nr + 1] = ''
			parts[nr + 2] = '\%s{%s}' % (method, incl_filename)
			continue
		""" first check if the file can be found """
		try:
			with open_from_dirs('%s.tex' % incl_filename, dirs) as fh:
				incl_content = fh.read()
		except OSError:
			if continue_on_missing:
				stderr.write('WARNING! "%s" could not be found! it was ignored\n' % incl_filename)
				parts[nr + 2] = ''
				if add_comments:
					parts[nr + 2] = '%% WARNING! \%s{%s} at this position ignored because it could not be found!' % (method, incl_filename)
				continue
			else:
				stderr.write('ERROR! "%s" could not be found! stopping\n' % incl_filename)
				exit(1)
		""" actual flattening """
		if method == 'input' or include_as_input:
			""" inputs simply include the text of the file (maybe we add a comment) replacing further inputs """
			prefix = '%% auto expanded for \%s{%s}:\n' % (method, incl_filename) if add_comments else ''
			if parts[nr + 1] == 'include':
				prefix += '% \include is treated as if it is \input\n'
			parts[nr + 2] = prefix if add_comments else ''
			parts[nr + 2] += _flatten_text(
				incl_content, dirs = dirs,
				add_comments = add_comments,
				include_as_input = include_as_input,
				continue_on_missing = continue_on_missing,
				includeonly = includeonly,
				exclude_extensions = exclude_extensions,
				is_included = is_included
			)
		elif method == 'include':
			""" includes create an empty page for the content, cannot be nested and may be restricted """
			if not includeonly or incl_filename in includeonly:
				if is_included:
					stderr.write('ERROR! "%s" could not be included because includes can not be nested! stopping\n' % incl_filename)
					exit(1)
				parts[nr + 2] = ''
				if add_comments:
					parts[nr + 2] += '%% auto expanded for \include{%s}\n%% (implicit \clearpage added explicitly)' % incl_filename
				parts[nr + 2] += '\n\clearpage\n%s\n\clearpage' % _flatten_text(
					incl_content, dirs = dirs,
					add_comments = add_comments,
					include_as_input = include_as_input,
					continue_on_missing = continue_on_missing,
					includeonly = includeonly,
					exclude_extensions = exclude_extensions,
					is_included = True
				)
			elif add_comments:
				parts[nr + 2] = '%% \include{%s} skipped because of \includeonly' % incl_filename
		parts[nr + 1] = ''  # hide instead of delete to preserve numbering

	return ''.join(parts)


if __name__ == '__main__':
	"""
		Invoked from command line.
	"""
	#todo: flags for other arguments
	if len(argv) <= 1:
		print 'welcome to FlattenTeX!'
		print 'please provide a path to a TeX file which you want to flatten'
		print 'optionally, you may provide a path, formatted as TEXINPUTS'
		print 'the flat document will be shown on stdout'
		exit()
	tex_file = argv[1]
	path = dirname(abspath(tex_file))
	if len(argv) >= 3:
		path = argv[2]
	elif 'TEXINPUTS' in environ:
		path = environ['TEXINPUTS']
	print flatten(tex_file = tex_file, path = path, include_as_input = False)


