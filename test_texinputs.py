
from os import chdir
from os.path import abspath, dirname, join
from texinputs import path_to_dirs, open_from_dirs


test_dir = join(abspath(dirname(__file__)), 'test_tex')


def test_path_to_dirs():
	"""
		Test that path_to_dirs adds directories in the correct order and recursively where needed.
	"""
	chdir(join(test_dir, 'subdirectory'))
	path = '.:%s//:' % test_dir
	dirs = path_to_dirs(path)
	assert 'test_tex' in dirs[0] and 'subdirectory' in dirs[0]
	assert any('external_subdirectory' in dir for dir in dirs)


def test_open_from_dirs():
	dirs = [
		join(test_dir, 'subdirectory'),
		join(test_dir, 'external'),
		join('/this/does/not/exist/42/'),
	]
	assert open_from_dirs('external_dependency.tex', dirs)
	try:
		assert open_from_dirs('samedir_dependency.tex', dirs)
	except OSError:
		""" as expected """
	else:
		raise AssertionError('opening a non-existent file should have raised an exception, but it did not')


if __name__ == '__main__':
	"""
		The adviced way to run tests is with py.test
		(simply type py.test in the main directory if it is installed)
	"""
	test_path_to_dirs()
	test_open_from_dirs()


