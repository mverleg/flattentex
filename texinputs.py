
"""
	Functions related to the TEXINPUTS environment variable.
"""


from genericpath import isfile
from os import getcwd, walk
from os.path import join, abspath, isdir


def path_to_dirs(path):
	"""
		Given a path string formatted like TEXINPUTS, return the directories included in that path
		(dependent on system and cwd).
	"""
	path_parts = path.split(':')
	if not len(path_parts[-1]):
		""" this means standard directories are searched, but you shouldn't have to \input anything from them """
		path_parts.pop(-1)
	directories = []
	for dr in path_parts:
		if dr.endswith('//'):
			""" this means the directory is to be searched recursively; expand this entry """
			dr = dr.rstrip('/')
			subdirs = [abspath(join(dr, entry[0])) for entry in walk(dr, followlinks = True) if isdir(join(dr, entry[0])) and not '/.' in entry[0]]
			directories.extend(subdirs)
		else:
			directories.append(dr)
	for k, dr in enumerate(directories):
		if dr.startswith('.'):
			""" this means current working directory """
			directories[k] = join(getcwd(), dr[1:])
	return directories


def open_from_dirs(filename, dirs = None, mode = 'r'):
	"""

		:param filename: the filename of the file to open
		:param dirs: directories to search for filename (first match is returned), e.g. like a path
		:param mode: as with open()
		:return: as with open()
		:raise OSError: if file not found in any of the directories
	"""
	if dirs is None:
		dirs = [getcwd()]
	for dr in dirs:
		filepath = abspath(join(dr, filename))
		if isfile(filepath):
			break
	else:
		raise OSError('the file "%s" was not found; tried "%s"' % (filename, '", "'.join(dirs)))
	return open(filepath, mode = mode)


