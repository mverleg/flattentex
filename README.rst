
FlattenTeX
------------

Includes the content of input and include statements in TeX files so that you have a single file (e.g. for sending to other people).

This project should be considered beta status. Bug reports are most welcome!

Features
------------

* Works with input
* Works with include (inserts page breaks and respecting the nesting limit)
* Respects includeonly
* Looks for files in TEXINPUTS correctly
* Not easily tricked by comments, extra whitespace etc
* Includes demo structure for testing with py.test

Use
------------

To use, call

	python2 flatex.py texfile.tex

This will use TEXINPUTS, if it is set in the environment. You can manually provide another path by

	python2 flatex.py texfile.tex "$MYPATH"

You can run the included tests with

	py.test

The testing project can be found in test_tex/. You can also build the test document directly with make, or flattened with make flat, to compare manually.



