
from flatex import flatten


def test_flatten():
	"""
		Tests all of flatten at once by applying it to the included
		test directory structure (given correct path).

		This tests at least that:

		* normal input and include work
		* input in the middle of line works
		* input across multiple lines works
		* commented input is ignored
		* recursive input works
		* TEXINPUTS is checked (incl recursion)
	"""
	include = [
		'external-sub-directory-dependency',
		'external-directory-dependency',
		'sub-directory-dependency',
		'difficult-name',
		'recursive-top',
		'recursive-middle',
		'recursive-bottom-1',
		'recursive-bottom-2',
		'same-directory-dependency',
		'noise-before',
		'noise-after',
		'before-comment',
		'after-comment',
		'fake-comment',
	]
	""" after-comment should be in .tex, not in .pdf """
	exclude = [
		'external-directory-unrelated-file',
		'sub-directory-unrelated-file',
		'same-directory-unrelated-file',
		'not-include-only',
	]
	flattened = flatten('test_tex/main.tex')
	for incl in include:
		assert incl in flattened
	for excl in exclude:
		assert excl not in flattened


if __name__ == '__main__':
	"""
		The adviced way to run tests is with py.test
		(simply type py.test in the main directory if it is installed)
	"""
	test_flatten()


